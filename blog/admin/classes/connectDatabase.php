<?php
// trida pro vytvoreni spojeni s SQL DB

class DBConnect {
    private $hostname, $username, $password, $db_name, $con;	

    // konstruktor tridy
    public function __construct($hostname, $username, $password, $db_name)
    {

        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
        $this->db_name = $db_name;
    }

	// pripojeni k SQL DB
    public function connectToMySQL()
    {
        $this->con = mysqli_connect($this->hostname, $this->username, $this->password, $this->db_name);
        if ($this->con) {
        	return $this->con;
        }else {
			die('Connect Error: ' . mysqli_connect_error());
			return false;
        }
    }
	// naszaveni znakove sady
	public function charset($chset){
		$is = mysqli_set_charset($this->con, $chset);
		if ($is == false) {
			return false;
		}
		else{
			return true;
		}
	}
	// spusteni query
	public function myquery($q){

		$result = mysqli_query($this->con, $q);
		if (!$result) {
			die('Error:' . mysqli_error($this->con));
    		return false;
    	}
    	else{
    		return $result;
    	}
	}
	
	// vytvoreni tabulek, pokud neexistuji
	public function admintables(){
  static $admin = "CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heslo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;";
 $this->myquery($admin);
												
  static $articles = "CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `headline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` TEXT COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL, 
  PRIMARY KEY (`id`)
  );";  
  $this->myquery($articles);

  static $category = "CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
  );";
  $this->myquery($category); 											
	}
// ukonceni/uzavreni spojeni s SQL DB
    function closeConnection()
    {
        mysqli_close($this->con);
    }
}
?>