<? ob_start(); ?>
<?php
require_once('../config.php');
require_once('../classes/connectDatabase.php');  
$con = new DBConnect(HOSTNAME, USER, PASSWORD, DB_NAME);
$conMySQL = $con->connectToMySQL();
$con->admintables();
session_start();      // start session stealing 
require_once 'checkuserfunction.php'; // kontrola přístupu 

//  ******set time_zone just for ME*****
        $q = "SET time_zone = '+01:00'";
        $result =  $con->myquery($q); 

// ********** MODE ADD ************   


$mode=$_GET["mode"];
if($mode=="add"){ 
	$headline=$_POST["headline"];
	$content=$_POST["content"];    
	if(isset($_POST['visible']) == YES){
    	$visible=$_POST["visible"];
    }
    else{
        $visible=NO;
    } 
         
    //Datainfo table 
    $keywords=$_POST["keywords"];
    $description=$_POST["description"];

    //Category table 
    if(($_POST['category']) == 'none'){
        //Kategorie nezvolena (none) a nezaložena jiná 
    	if(empty ($_POST['categoryname'])){
        	$categorynone=none;

          $q="insert into articles (category,headline,content,visible,deleted,keywords,description,date) values".
             	" ('$categorynone','$headline','$content','$visible','ne','$keywords','$description', ". 
             	"DATE_FORMAT(NOW(),'%d-%m-%Y %H:%i'))";
             $result =  $con->myquery($q);             
            header("location: index.php?id=$categorynone");
        }
            
        //Vložení nové kategorie
        else{
            $categoryname=$_POST["categoryname"];
            $categorydescription=$_POST["categorydescription"];
    
            $q="insert into category (name, description) values ('$categoryname','$categorydescription')";
            // priprava query spusteni
            if ($stmt = mysqli_prepare($conMySQL, $q)){
            	// query spusteni
            	mysqli_stmt_execute($stmt);
            	// presun vysledku
                mysqli_stmt_store_result($stmt);
            } 
            // ID posledni vlozeneho zaznamu
            $catid = $stmt->insert_id;   
           
             $q="insert into articles (category,headline,content,visible,deleted,keywords,description,date)".
               " values ('$catid','$headline','$content','$visible','ne','$keywords','$description', DATE_FORMAT(NOW(),'%d-%m-%Y %H:%i'))";
             $result =  $con->myquery($q);            
        }
        header("location: index.php?id=$catid");
    }      
          
        //vybraná kategorie z již existujících
    else{
        $category=$_POST["category"];
        
        
       $q="insert into articles (category,headline,content,visible,deleted,keywords,description,date)values ".
         	"('$category','$headline','$content','$visible','ne','$keywords','$description', ".
         	" DATE_FORMAT(NOW(),'%d-%m-%Y %H:%i'))";
        $result =  $con->myquery($q);  
        header("location: index.php?id=$category");
    }     
}
// ********** MODE UPDATE ************      
elseif($mode=="update")  { 
        $headline=$_POST["headline"];
    	$articleid=$_POST["id"];
	    $content=$_POST["content"];    
		    
        if(isset($_POST['visible']) == YES){          
            $visible=YES;
        }
        else{
            $visible=NO;
        } 
         
         //Datainfo table 
        $keywords=$_POST["keywords"];
        $description=$_POST["description"];

        //Category table 
        if(($_POST['category']) == 'none'){
            //Kategorie nezvolena (none) a nezaložena jiná 
            if(empty ($_POST['categoryname'])){
                $categorynone="none";
                                                       
                $q="update articles set category='$categorysne', headline='$headline', content='$content', ".
                	"visible='$visible',keywords='$keywords',description='$description'," . 
                	"updatedate=DATE_FORMAT(NOW(),'%d-%m-%Y %H:%i') where id = $articleid;";
                $result =  $con->myquery($q);             
                header("location: index.php?id=$categorynone");
             }
            
            //Vložení nové kategorie
            else {
                $categoryname=$_POST["categoryname"];
                $categorydescription=$_POST["categorydescription"];
    
                $q="insert into category (name, description) ".
                	"values ('$categoryname','$categorydescription')";
                if ($stmt = mysqli_prepare($conMySQL, $q)){
                     mysqli_stmt_execute($stmt);
                     mysqli_stmt_store_result($stmt);
                } 
                $catid = $stmt->insert_id;   

                $q="update articles set category='$catid',headline='$headline',content='$content'," .
                	"visible='$visible',keywords='$keywords',description='$description',".
                	"updatedate=DATE_FORMAT(NOW(),'%d-%m-%Y %H:%i') where id = $articleid;";
                $result =  $con->myquery($q);            
            }
            header("location: index.php?id=$catid");
          }      
          
        //vybraná kategorie z již existujících
        else{
        	$category=$_POST["category"];               
          	$q="update articles set category='$category',headline='$headline',content='$content'," .
          		"visible='$visible',keywords='$keywords',description='$description',".
          		"updatedate=DATE_FORMAT(NOW(),'%d-%m-%Y %H:%i') where id = $articleid;";
          	$result =  $con->myquery($q);  
          	header("location: index.php?id=$category");
        }
         
	}?>
      <? ob_flush(); ?>