  <?php
require_once('../config.php');  
require_once('../classes/connectDatabase.php');
$con = new DBConnect(HOSTNAME, USER, PASSWORD, DB_NAME);
$conMySQL = $con->connectToMySQL();
$con->admintables();
session_start();      // start session stealing 
require_once 'checkuserfunction.php'; // kontrola přístupu 
?>



                
<form name="form1" method="post" action="formsubmit.php?mode=add">
                     
  <h1>Přidání článku</h1>    
                  
  <a href="index.php">Zpět</a>
                  
  <table>
    <tr>
      <td>Nadpis</td>
      <td><input class="okna" type="text" name="headline" onfocus="this.select()"></td>
       <td>      
           <b>Nová kategorie</b>   
       </td>
    </tr>
    
    <tr> 
      <td>Kategorie</td>
      <td>
        <select name="category" >
          <option value="none">none</option> 
            <?php
            $q="SELECT id, name, description FROM category";
            $result =  $con->myquery($q);
                      
            while($mydata = mysqli_fetch_array($result))
            {            // make an option 
              $categorydata = '<option value="'.$mydata['id'].'"';         //<option value="id"  
              $categorydata .= ' title = "'.$mydata['description'].'"';    //title = "description"
              $categorydata .= ' >';                                       // >
              $categorydata .= $mydata['name'].'</option>';                //name</option>
            
            
            echo $categorydata; 
            
            }  
            ?>  
          </select>           
       </td>
       <td>
           Název<input class="okna" type="text" name="categoryname" onfocus="this.select()">
           Popis<input class="okna" type="text" name="categorydescription" onfocus="this.select()">       
       </td>
    </tr>
     
    <tr>
      <td>Obsah</td>
      <td><textarea name="content"></textarea></td>
    </tr>
    <tr>
      <td>Publikovat</td>
      <td> <input type="checkbox" name= "visible" value="<?php echo YES; ?>"></td>
    </tr>  
  
    <tr>
      <td>
      <h2>SEO</h2>
      </td>
      <td> 
      <i>SEO - optimalizace pro vyhledávače<i>         
      <p><i>Kvůli zlepšení optimalizace můžete vyplnit "klíčová slova" a "popis". Klíčová slova volte v závislosti na obsahu článku. Maximální počet klíčových slov je 25. V popisu napište krátký popis o čem Váš článek je.</i></p>
      </td>        
      </tr>
         
      <tr>
      <td>Klíčová slova</td>
      <td><input class="okna" type="text" name="keywords" onfocus="this.select()"></td>
      </tr>
      
  
      <tr>
      <td>Popis</td>
      <td><input class="okna" type="text" name="description" onfocus="this.select()"></td>
      </tr>
  
  
  </table>
  
  <input id="submit" type="submit" name="Submit" value="Uložit">

</form>