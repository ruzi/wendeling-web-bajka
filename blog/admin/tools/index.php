<?php
require_once('../../admin/config.php');
require_once('../../admin/classes/connectDatabase.php');
  
$con = new DBConnect(HOSTNAME, USER, PASSWORD, DB_NAME);
$conMySQL = $con->connectToMySQL();
$con->admintables();
session_start();      // start session stealing 
require_once 'checkuserfunction.php'; // kontrola přístupu 


/* DEFINICE začátku tabulky (OBECNÉ)*/
            $tablefirstpart = '<table> ';
            $tablefirstpart .= '<thead>' ;
            $tablefirstpart .= '<tr><td>Nadpis</td><td>Datum</td><td>Zobrazeni</td><td></td></tr>' ;       
            $tablefirstpart .= '</thead>'   ;
            $tablefirstpart .= '<tbody>'   ;
/* DEFINICE začátku tabulky (just for deleted table)*/            
            $tablefirstpartfordeleted = '<table> ';
            $tablefirstpartfordeleted .= '<thead>' ;
            $tablefirstpartfordeleted .= '<tr><td>Nadpis</td><td>Datum</td><td></td></tr>' ;       
            $tablefirstpartfordeleted .= '</thead>'   ;
            $tablefirstpartfordeleted .= '<tbody>'   ;
/* DEFINICE začátku tabulky (just for Categories EDIT table)*/            
      		  $tablefirstpartforcategoriesedit = '<table> ';
            $tablefirstpartforcategoriesedit .= '<thead>' ;
            $tablefirstpartforcategoriesedit .= '<tr><td>Název</td><td>Popis</td><td></td></tr>' ;       
            $tablefirstpartforcategoriesedit .= '</thead>'   ;
            $tablefirstpartforcategoriesedit .= '<tbody>'   ;
/* Definice headline of deletedfiles*/
            $headlinefordeletedfiles = '<h3>Články v koši</h3>'; 
/* Definice headline of Categories EDIT */
            $headlineforcategoriesedit = '<h3>Kategorie</h3>';
?>   
                     
<?php include ("./hlavicka.php"); ?>                              
<?php include ("./menu.php"); ?>                              
<?php include ("./obsah-pravo.php"); ?> 
 
            <div id="obsah_stranky">
                <article>


    <p>Přihlášen jako: <b><?php echo $_SESSION['email']; ?></b>  <a href="../login.php?logout">[Odhlásit]</a></p>
      
    <h1>Administrace stránek</h1>
    
    <p><a href="akce.php?mode=add">Nový článek</a> - <a href="index.php?mode=deletedfiles">Koš</a></p>
    
    <h2>Kategorie</h2>  
    <ul>
        <a href ="index.php?id=''"> <li>Nezařazeno</li></a>        
                        <?php
                        /*********************LIST FOR CATEGORIES*************/   
                        $q="SELECT id, name, description FROM category";
                        $result =  $con->myquery($q);
                        
                        if (!mysqli_num_rows($result) == 0) {           
                        	while($mydataforcategories = mysqli_fetch_array($result)){             
                          		$categorydata = '<a href ="index.php?id="'. 
                          			$mydataforcategories['id'] . 
                          			'"><li>' .          
                          			$mydataforcategories['name'] .
                          			'</li></a>';                
                          		echo $categorydata.PHP_EOL; 
                        	}
                        }
                  		else{
                  	       	echo '<b><i>Momentálně zde nejsou žádné kategorie.</i></b>';
                        }                      
                        ?> 
            </ul>
            <a href="index.php?mode=categoryedit">Upravit kategorie</a>

      <?php 
    
      $mode=$_GET["mode"];
		  /********************LIST FOR DELETED FILES***********************/
      if($mode=="deletedfiles") { 
        $q="select id, headline, date, deleted  from articles where deleted = 'ano' ORDER BY date DESC"; // od nejnovejsiho
      	$resultfordeletedfiles =  $con->myquery($q);
        
        if (!mysqli_num_rows($resultfordeletedfiles) == 0) {
                echo $headlinefordeletedfiles; 
                echo $tablefirstpartfordeleted;
                //vrací odkaz na pole
              	while($mydatafordeletedfiles = mysqli_fetch_array($resultfordeletedfiles, MYSQLI_ASSOC)){
	                $articlesdata = '<tr><td>'.$mydatafordeletedfiles['headline'].'</td>';
    	            $articlesdata .= '<td>'.$mydatafordeletedfiles['date'].'</td>';
        	        $articlesdata .= '<td><a href ="akce.php?id='.$mydatafordeletedfiles['id'].'&mode=update"><img alt="edit" src="../style/edit.png" width="16" height="16"></a> ';
            	    $articlesdata .= '<a href="smazat.php?id='.$mydatafordeletedfiles['id'].'&mode=refresh" onclick="return confirm(\'Obnova clanku z kose.\')">  <img alt="edit" src="../style/refresh.png" width="16" height="16"></a></tr>';
                	echo $articlesdata.PHP_EOL;
                }
              	echo '</tbody></table>'   ; 
        } 
        else{
        	echo '<br><b><i>Koš je prázdný!</i></b>';
        } 
      }
		  /********************LIST FOR EDIT CATEGORIES***********************/        
      if($mode=="categoryedit") { 
      		$q="select id, name, description from category"; // od nejnovejsiho
      		$resultforeditcat =  $con->myquery($q);
        
        	if (!mysqli_num_rows($resultforeditcat) == 0) {
                echo $headlineforcategoriesedit; 
                echo $tablefirstpartforcategoriesedit;
                //vrací odkaz na pole
              	while($mydataforeditcat = mysqli_fetch_array($resultforeditcat, MYSQLI_ASSOC)){
	                $editcategoriesdata = '<tr><td>'.$mydataforeditcat['name'].'</td>';
    	            $editcategoriesdata .= '<td>'.$mydataforeditcat['description'].'</td>';
        	        $editcategoriesdata .= '<td><a href ="catedit.php?id='.$mydataforeditcat['id'].'&mode=update"><img alt="edit" src="../style/edit.png" width="16" height="16"></a> ';
            	    $editcategoriesdata .= '<a href="catedit.php?id='.$mydataforeditcat['id'].'&mode=delete" onclick="return confirm(\'Smazat kategorii: '.$mydataforeditcat['name'].'\')">  <img alt="edit" src="../style/smaz.png" width="16" height="16"></a></tr>';
                	echo $editcategoriesdata.PHP_EOL;
                }
              	echo '</tbody></table>'   ; 
            } 
        	else{
        	  echo '<br><b><i>Neexistují žádné kategorie!</i></b>';
        	} 
        }        
      	else  {
      /*********************TABLE FOR ARTICLES*************/       		
            if (isset ($_GET['id'])) //když ?id=x
            {
      			$urlid=$_GET["id"];
      			$q="select id, headline, visible, deleted, date from articles where category='$urlid' and deleted = 'ne' ORDER BY date DESC"; // od nejnovejsiho
      			$resultforarticles =  $con->myquery($q);
      
      			 
      			 if (!mysqli_num_rows($resultforarticles) == 0) { 
      			 $q="SELECT id, name FROM category where id ='$urlid'  ";
             $result =  $con->myquery($q);
             while($mydatafromcategory = mysqli_fetch_array($result, MYSQLI_ASSOC))//vrací odkaz na pole 
                {$headlineforarticles = '<h3>'.$mydatafromcategory['name'].'</h3>';
                echo $headlineforarticles.PHP_EOL;
                }

             
             
             echo $tablefirstpart ;
      
               while($mydataforarticles = mysqli_fetch_array($resultforarticles, MYSQLI_ASSOC))//vrací odkaz na pole 
               { 
               if ($mydataforarticles['visible'] == ne){$visible= ''.$mydataforarticles['visible'].'';}
               else {$visible= $mydataforarticles['visible'];} 
                                     
                 $articlesdata = '<tr><td>'.$mydataforarticles['headline'].'</td>';          
                 $articlesdata .= '<td>'.$mydataforarticles['date'].'</td><td>'.$visible.'</td>';              
                 $articlesdata .= '<td><a href ="akce.php?id='.$mydataforarticles['id'].'&mode=update"><img alt="edit" src="../style/edit.png" width="16" height="16"></a> '; 
                 $articlesdata .= '<a href="smazat.php?id='.$mydataforarticles['id'].'&mode=delete" onclick="return confirm(\'Tento článek bude vyhozen do koše!\')">  <img alt="edit" src="../style/smaz.png" width="16" height="16"></a></td></tr>';
                 echo $articlesdata.PHP_EOL; 
               } 
               
               echo '</tbody></table>'   ;
               }      
             
      			
      			 else {
      			       echo '<br><b><i>V této kategorii nejsou žádné články!</i></b>';
             }
      	     } 
            else{ //když neni zvelená kategorie 
            	echo "";
			} 
    		
    		}// else mode end
        ?>
    
    </div>
    </div>
    
          </article> 
         </div>
        </div>     
    </body>
</html>