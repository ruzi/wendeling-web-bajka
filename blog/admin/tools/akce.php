<?php
require_once('../../admin/config.php');
require_once('../../admin/classes/connectDatabase.php');
  
$con = new DBConnect(HOSTNAME, USER, PASSWORD, DB_NAME);
$conMySQL = $con->connectToMySQL();
$con->admintables();
session_start();      // start session stealing 
require_once 'checkuserfunction.php'; // kontrola přístupu 
?>

 <?php include ("./hlavicka.php"); ?>                              
 <?php include ("./menu.php"); ?>                              
 <?php include ("./obsah-pravo.php"); ?>                              
       <div id="obsah_stranky">
	       <article>
  		   <?php
           		$mode=$_GET["mode"];
          		if($mode=="add") { 
                	include 'modeadd.php';
                } 
                else{
                	include 'modeupdate.php';    	  
                }                  
          	?>
           </article> 
         </div>
        </div>     
    </body>
</html>

