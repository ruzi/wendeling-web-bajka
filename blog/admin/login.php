<?php 
session_start();
require_once dirname(__FILE__)."/config.php";
require_once('classes/connectDatabase.php');
$con = new DBConnect(HOSTNAME, USER, PASSWORD, DB_NAME);
$conMySQL = $con->connectToMySQL();
$con->admintables();

// registrace sessions 
if (!session_is_registered("email"))
    session_register("email");

if (!session_is_registered("heslo"))
    session_register("heslo");

// odeslání login dat a kontrola vyskytu v DB     
if (isset($_POST['submit'])) {
    $q="SELECT 'id' FROM admin WHERE email='".addslashes($_POST['email'])."' AND heslo='".md5(trim($_POST['heslo']))."'";
    
    if ($stmt = mysqli_prepare($conMySQL, $q)) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        }
    
    if (mysqli_stmt_num_rows($stmt)==1) { # kontrola jmena a hesla
        session_regenerate_id(); # osetreni session stealing 
        $_SESSION['email'] = $_POST['email']; // nastavení sessions 
        $_SESSION['heslo'] = md5($_POST['heslo']);
        header("Location: ./tools/index.php");
        }
        
    else { # 401 ERORR
        header("Location: ./login.php?code=401", 401);
        } 
}

// pri odlogování smazani session
if (isset($_GET['logout'])) { 
    unset($_SESSION['email']);
    unset($_SESSION['heslo']);
    session_destroy();
}

// Změta  $title při chybné stránce (a nastavení defaultní ho title)
isset($_GET['code']) && $_GET['code']=="401" ? # Titulek
   $title = "<span class='chybareg'>Špatné heslo!</span>" :
   $title = "Přihlásit";
 ?>  

 <?php include ("tools/hlavicka.php"); ?>
                        


          <nav>
          <a href="../../"><img src="../../logo.png" alt="Logo čajovny" title="Úvodní stránka"></a>
                <ul>
                    <li><a href="../../">Úvod</a>
                    </li>
                    <li><a href="../../o-nas/">O nás</a>
                    </li>
                    <li><a href="../../nase-nabidka/">Naše nabídka</a>
                    </li>
                    <li><a href="../../akce/">Akce</a>
                    </li>
                    <li><a href="../../blog/">Blog</a>
                    </li>
                    <li><a href="../../kontakt/">Kontakt</a>
                    </li>                
                </ul>
            </nav>
            <div id="nadpis">
                <span id="bajka">Bajka</span>
                <span id="slogan">místo, kde si můžete vychutnat svůj oblíbený čaj i skvělé koktejly</span>
            </div>
<?php include ("tools/obsah-pravo.php"); ?> 
            <div id="obsah_stranky">
                <article>
                

      <form action="#" method="post">
        <?php if (isset($_GET['logout'])) { ?> <span class='logout'>Byl jste odhlášen</span><?php } ?>
        <h1><?php echo $title ?></h1>
        <p><input name="email" size="20" tabindex="1" type="text" /> <label>Email</label></p>
        <p><input name="heslo" size="20" tabindex="2" type="password" /> <label>Heslo</label></p>
        <p><input id="submit" name="submit" type="submit" tabindex="3" value=" Přihlásit &raquo; " /></p>
      </form>
 

          </article>
            </div>
        </div>     

    </body>
</html>