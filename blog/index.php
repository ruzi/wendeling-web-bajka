<?php
require_once('admin/config.php');
require_once('admin/classes/connectDatabase.php');
  
$con = new DBConnect(HOSTNAME, USER, PASSWORD, DB_NAME);
$conMySQL = $con->connectToMySQL();
$con->admintables();

?>

<?php include ("admin/tools/hlavicka.php"); ?>
    <a class="prihlasit" href="admin/login.php">Přihlásit</a>  
          <nav>
          <a href="../"><img src="../logo.png" alt="Logo čajovny" title="Úvodní stránka"></a>
                <ul>
                    <li><a href="../">Úvod</a>
                    </li>
                    <li><a href="../o-nas/">O nás</a>
                    </li>
                    <li><a href="../nase-nabidka/">Naše nabídka</a>
                    </li>
                    <li><a href="../akce/">Akce</a>
                    </li>
                    <li><a class="zvyrazni" href="#">Blog</a>
                    </li>
                    <li><a href="../kontakt/">Kontakt</a>
                    </li>                
                </ul>
            </nav>
            <div id="nadpis">
                <span id="bajka">Bajka</span>
                <span id="slogan">místo, kde si můžete vychutnat svůj oblíbený čaj i skvělé koktejly</span>
            </div>  
                  
<?php include ("admin/tools/obsah-pravo.php"); ?> 
            
            <div id="obsah_stranky">
	            <article>
 
<?php
if (isset ($_GET['id'])){ //když ?id=x
      // je url adrese id clanku?      
      $urlid=$_GET["id"];
      $q="select id, headline, content, description, visible, deleted, date from articles where " .
      	"category='$urlid' and deleted = 'ne' and visible='ano' ORDER BY date ASC"; // od nejnovejsiho
      $resultforarticles =  $con->myquery($q);     
      if (!mysqli_num_rows($resultforarticles) == 0){
          $q="SELECT id, name FROM category where id ='$urlid'  ";
          $resultcatname =  $con->myquery($q);
          if (!mysqli_num_rows($resultcatname) == 0) {
              while($mydatafromcategory = mysqli_fetch_array($resultcatname, MYSQLI_ASSOC)){ //vrací odkaz na pole 
                    $headlineforarticles = '<a href = "index.php" >&laquo; Zpět</a><h1>'.$mydatafromcategory['name'].'</h1>';
                    echo $headlineforarticles.PHP_EOL;
              }// end while 
          }//end if
          else{
          	echo '<h1>Články</h1>';
      	  }//end else
          echo '<ul class="clanky">'; 
                  
      	  while($mydataforarticles = mysqli_fetch_array($resultforarticles)){
       			$date = date("d.m.Y", strtotime($mydataforarticles['date']));
                                    
            	$articlesdate = '<li><a href="view.php?id='.$mydataforarticles['id'].'">'.PHP_EOL;      
            	$articlesdate .= ''.PHP_EOL;
            	$articlesdate .= '<h2>'.$mydataforarticles['headline'].'</h2>'.PHP_EOL;
            	$articlesdate .= '<span class="datum"><i>'.$date.':</i></span>'.PHP_EOL;                 
                 
            	$string = strip_tags($mydataforarticles['content']);

            	if (strlen($string) > 200){        
            		$stringCut = substr($string, 0, 200);
                	$articleview = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
            	}//end if
            	$articlesdate .= '';
            	$articlesdate .= $articleview.'</a></li>'.PHP_EOL;
            	echo $articlesdate.PHP_EOL;          
      	  }//end while
      	  echo '</ul>';
      }// end if
      else{
      	  echo '<b><i>Omlouváme se, kategorie ale ještě vytvořeny.</i></b>';
      }
}// end if
else{ // jestli neni v url id clanku
      $q="SELECT id, name, description FROM category";
	  $result =  $con->myquery($q);
 	  if (!mysqli_num_rows($result) == 0){  
      	  echo '<h1>Kategorie článků</h1><ul class="kategorie">';         
          while($mydataforcategories = mysqli_fetch_array($result)){
        	  $categorydata = '<li><a href="index.php?id='.$mydataforcategories['id'].'">';            
     	      $categorydata .= '<h2 class="menu-main">'.$mydataforcategories['name'].'</h2>';          
        	  $categorydata .= '<h3 class="menu-sub">'.$mydataforcategories['description'].'</h3>';
    	      $categorydata .= '</a></li>';              
 	         echo $categorydata.PHP_EOL; 
          }
	      echo '</ul>';
      }
	  else{
      	  echo '<b><i>Omlouváme se, ale nejsou zde žádné články.</i></b>';
      }                      
}//end else
?> 
          </article>
        </div>
       </div>     
   </body>
</html>